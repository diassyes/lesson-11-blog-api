package domain

import "github.com/google/uuid"

// ///////////////////////////////
// Main structure & methods
// ///////////////////////////////
type Post struct {
	Uid              string   `json:"uid,omitempty"`
	CategoryId       string   `json:"category_id,omitempty"`
	AuthorId         string   `json:"author_id,omitempty"`
	Title            string   `json:"title,omitempty"`
	ShortDescription string   `json:"short_description,omitempty"`
	PreviewUrl       string   `json:"preview_url,omitempty"`
	Content          string   `json:"content,omitempty"`
	Tags             []string `json:"tags,omitempty"`
}

// Generate post unique ID
func (p *Post) GenerateUid() {
	p.Uid = uuid.New().String()
}

// ///////////////////////////////
// Repository interfaces
// ///////////////////////////////
type PostCommandRepo interface {
	Store(post *Post) error
	DeleteById(id string) error
}

type PostQueryRepo interface {
	Store(post *Post) error
	DeleteById(id string) error
	GetById(id string) (*Post, error)
	GetByFilter(post *Post) (*[]Post, error)
}
