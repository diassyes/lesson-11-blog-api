package blog

import (
	"context"
	"encoding/json"
	kithttp "github.com/go-kit/kit/transport/http"
	"github.com/gorilla/mux"
	"gitlab.com/golang-demos/blog-api/src/errors"
	"net/http"
)

// Http routes handlers
func MakeHandler(ss Service) http.Handler {

	// Options array
	options := []kithttp.ServerOption{
		kithttp.ServerErrorEncoder(encodeError),
	}

	// Blog routes handlers
	savePost := kithttp.NewServer(
		makeSavePostEndpoint(ss),
		decodeSavePostRequest,
		encodeResponse,
		options...,
	)

	getFilteredPosts := kithttp.NewServer(
		makeGetFilteredPostsEndpoint(ss),
		decodeGetFilteredPostsRequest,
		encodeResponse,
		options...,
	)

	getPost := kithttp.NewServer(
		makeGetPostEndpoint(ss),
		decodeGetPostRequest,
		encodeResponse,
		options...,
	)

	deletePost := kithttp.NewServer(
		makeDeletePostEndpoint(ss),
		decodeDeletePostRequest,
		encodeResponse,
		options...,
	)

	// Init router
	r := mux.NewRouter()

	// Blog routes
	r.Handle("/blog-api/post", savePost).Methods("PUT", "POST")
	r.Handle("/blog-api/post", getFilteredPosts).Methods("GET")
	r.Handle("/blog-api/post/{id}", getPost).Methods("GET")
	r.Handle("/blog-api/post/{id}", deletePost).Methods("DELETE")

	return r
}

// Blog request decoders ------------------------------------

// Create/Update post request decoder
func decodeSavePostRequest(_ context.Context, r *http.Request) (interface{}, error) {

	var body savePostRequest

	// Parse request
	err := json.NewDecoder(r.Body).Decode(&body)
	if err != nil {
		return nil, errors.InvalidCharacter.DevMessage(err.Error())
	}

	// Validate request
	if body.CategoryId == "" {
		return nil, errors.InvalidCharacter.DevMessage("post category id must be provided")
	}
	if body.AuthorId == "" {
		return nil, errors.InvalidCharacter.DevMessage("post author id must be provided")
	}
	if body.Title == "" {
		return nil, errors.InvalidCharacter.DevMessage("post title must be provided")
	}
	if body.Content == "" {
		return nil, errors.InvalidCharacter.DevMessage("post content must be provided")
	}

	return body, nil
}

// Get post request decoder
func decodeGetFilteredPostsRequest(_ context.Context, r *http.Request) (interface{}, error) {

	var body getFilteredPostsRequest

	err := json.NewDecoder(r.Body).Decode(&body)
	if err != nil {
		return nil, errors.InvalidCharacter.DevMessage(err.Error())
	}

	return body, nil
}

// Get filtered posts request decoder
func decodeGetPostRequest(_ context.Context, r *http.Request) (interface{}, error) {

	var body getPostRequest
	vars := mux.Vars(r)

	id, ok := vars["id"]
	if !ok {
		return nil, errors.NoFound
	}

	body.PostId = id

	return body, nil
}

// Delete post request decoder
func decodeDeletePostRequest(_ context.Context, r *http.Request) (interface{}, error) {
	var body deletePostRequest
	vars := mux.Vars(r)

	id, ok := vars["id"]
	if !ok {
		return nil, errors.NoFound
	}

	body.PostId = id

	return body, nil
}

// Response encoders
func encodeResponse(ctx context.Context, w http.ResponseWriter, response interface{}) error {
	e, ok := response.(errorer)
	if ok && e.error() != nil {
		encodeError(ctx, e.error(), w)
		return nil
	}
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	return json.NewEncoder(w).Encode(response)
}

// Error response encoder
type errorer interface {
	error() error
}

func encodeError(_ context.Context, err error, w http.ResponseWriter) {
	e, ok := err.(*errors.ArgError)
	if ok {
		w.WriteHeader(e.Status)
	} else {
		w.WriteHeader(http.StatusInternalServerError)
	}
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	_ = json.NewEncoder(w).Encode(err)
}
