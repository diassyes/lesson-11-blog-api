package blog

import (
	"context"
	"github.com/go-kit/kit/endpoint"
	"gitlab.com/golang-demos/blog-api/src/domain"
)

// Blog endpoints

// Create/Update post req & resp
type savePostRequest struct {
	domain.Post
}

type savePostResponse struct {
	PostId string `json:"post_id"`
}

// Create/Update post endpoint
func makeSavePostEndpoint(s Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(savePostRequest)
		resp, err := s.SavePost(&req)
		if err != nil {
			return nil, err
		}

		return resp, nil
	}
}

// Get filtered posts req & resp
type getFilteredPostsRequest struct {
	domain.Post
}

type getFilteredPostsResponse struct {
	Posts []domain.Post
}

// Get post endpoint
func makeGetFilteredPostsEndpoint(s Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(getFilteredPostsRequest)
		resp, err := s.GetFilteredPosts(&req)
		if err != nil {
			return nil, err
		}

		return resp, nil
	}
}

// Get post req & resp
type getPostRequest struct {
	PostId string `json:"post_id"`
}

type getPostResponse struct {
	domain.Post
}

// Get post endpoint
func makeGetPostEndpoint(s Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(getPostRequest)
		resp, err := s.GetPost(&req)
		if err != nil {
			return nil, err
		}

		return resp, nil
	}
}

// Delete post req & resp
type deletePostRequest struct {
	PostId string `json:"post_id"`
}

type deletePostResponse struct {
	Msg string `json:"msg"`
}

// Delete post endpoint
func makeDeletePostEndpoint(s Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(deletePostRequest)
		resp, err := s.DeletePost(&req)
		if err != nil {
			return nil, err
		}

		return resp, nil
	}
}
