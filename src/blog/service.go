package blog

import (
	"gitlab.com/golang-demos/blog-api/src/domain"
)

// //////////////////////////////////////
// Main service interface & constructor
// //////////////////////////////////////
type Service interface {
	SavePost(req *savePostRequest) (*savePostResponse, error)
	GetPost(req *getPostRequest) (*getPostResponse, error)
	GetFilteredPosts(req *getFilteredPostsRequest) (*getFilteredPostsResponse, error)
	DeletePost(req *deletePostRequest) (*deletePostResponse, error)
}

type service struct {
	postCommandRepo domain.PostCommandRepo
	postQueryRepo   domain.PostQueryRepo
}

func NewService(postCR domain.PostCommandRepo, postQR domain.PostQueryRepo) Service {
	return &service{
		postCommandRepo: postCR,
		postQueryRepo:   postQR,
	}
}

// Blog service methods ------------------------------------------------------

// Create/Update post service method
func (s *service) SavePost(req *savePostRequest) (*savePostResponse, error) {

	post := req.Post

	if post.Uid == "" {
		post.GenerateUid()
	}

	err := s.postCommandRepo.Store(&post)
	if err != nil {
		return nil, err
	}

	err = s.postQueryRepo.Store(&post)
	if err != nil {
		return nil, err
	}

	return &savePostResponse{PostId: post.Uid}, nil
}

// Get filtered posts service method
func (s *service) GetFilteredPosts(req *getFilteredPostsRequest) (*getFilteredPostsResponse, error) {
	posts, err := s.postQueryRepo.GetByFilter(&req.Post)
	if err != nil {
		return nil, err
	}

	return &getFilteredPostsResponse{*posts}, nil
}

// Get post service method
func (s *service) GetPost(req *getPostRequest) (*getPostResponse, error) {

	post, err := s.postQueryRepo.GetById(req.PostId)
	if err != nil {
		return nil, err
	}

	return &getPostResponse{*post}, nil
}

// Delete post service method
func (s *service) DeletePost(req *deletePostRequest) (*deletePostResponse, error) {

	err := s.postCommandRepo.DeleteById(req.PostId)
	if err != nil {
		return nil, err
	}

	err = s.postQueryRepo.DeleteById(req.PostId)
	if err != nil {
		return nil, err
	}

	return &deletePostResponse{Msg: "post deleted successfully"}, nil
}
