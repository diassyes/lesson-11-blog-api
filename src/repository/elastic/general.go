package elastic

import (
	"github.com/olivere/elastic/v7"
	"gitlab.com/golang-demos/blog-api/src/config"
	"gitlab.com/golang-demos/blog-api/src/errors"
)

func ConnectionStart() (*elastic.Client, error) {
	client, err := elastic.NewClient(
		elastic.SetURL(config.AllConfigs.Elastic.ConnectionIp...),
		elastic.SetSniff(false),
	)
	if err != nil {
		return nil, errors.ElasticConnectError.DevMessage(err.Error())
	}

	return client, nil
}
