package elastic

import (
	"context"
	"encoding/json"
	"github.com/olivere/elastic/v7"
	"gitlab.com/golang-demos/blog-api/src/domain"
	"gitlab.com/golang-demos/blog-api/src/errors"
)

type postQueryRepo struct {
	client *elastic.Client
}

func NewPostQueryRepo(client *elastic.Client) domain.PostQueryRepo {
	return &postQueryRepo{client: client}
}

func (r *postQueryRepo) Store(post *domain.Post) error {

	_, err := r.client.Index().
		Index("blog").
		Type("post").
		Id(post.Uid).
		BodyJson(post).
		Refresh("true").
		Do(context.TODO())

	return err
}

func (r *postQueryRepo) GetByFilter(post *domain.Post) (*[]domain.Post, error) {
	query := elastic.NewBoolQuery()

	if post.CategoryId != "" {
		query = query.Must(elastic.NewTermQuery("category_id.keyword", post.CategoryId))
	}
	if post.AuthorId != "" {
		query = query.Must(elastic.NewTermQuery("author_id.keyword", post.AuthorId))
	}
	if post.Title != "" {
		query = query.Must(elastic.NewTermQuery("title.keyword", post.Title))
	}
	if post.ShortDescription != "" {
		query = query.Must(elastic.NewTermQuery("short_description.keyword", post.ShortDescription))
	}
	if post.Content != "" {
		query = query.Must(elastic.NewTermQuery("content.keyword", post.Content))
	}
	if len(post.Tags) > 0 {
		for _, tag := range post.Tags {
			query = query.Filter(elastic.NewMatchQuery("tags.keyword", tag))
		}
	}
	search, err := r.client.Search().
		Index("blog").
		Query(query).
		Do(context.TODO())

	if err != nil {
		return nil, err
	}

	var posts []domain.Post
	if search.Hits.TotalHits.Value > 0 {
		for _, hit := range search.Hits.Hits {
			var post domain.Post
			err := json.Unmarshal(hit.Source, &post)
			if err != nil {
				return nil, err
			}
			posts = append(posts, post)
		}
	} else {
		errors.NoContentFound.DeveloperMessage = "No resource"
		return nil, errors.NoContentFound
	}

	return &posts, nil
}

func (r *postQueryRepo) GetById(id string) (*domain.Post, error) {
	get, err := r.client.Get().
		Index("blog").
		Id(id).
		Do(context.TODO())

	if err != nil {
		errors.NoContentFound.DeveloperMessage = "No resource"
		return nil, errors.NoContentFound
	}

	var post domain.Post

	if get.Found {
		err := json.Unmarshal(get.Source, &post)
		if err != nil {
			return nil, err
		}
	} else {
		return nil, err
	}
	return &post, err
}

func (r *postQueryRepo) DeleteById(id string) error {
	_, err := r.client.Delete().
		Index("blog").
		Type("post").
		Id(id).
		Do(context.TODO())
	if err != nil {
		return err
	}
	return err
}
