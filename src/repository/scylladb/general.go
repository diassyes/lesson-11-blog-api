package scylladb

import (
	"github.com/gocql/gocql"
	"gitlab.com/golang-demos/blog-api/src/config"
	"gitlab.com/golang-demos/blog-api/src/errors"
	"time"
)

/////////////////////////////////
// Main connection functions
/////////////////////////////////

// Init ScyllaDB connection
func ConnectionStart() (*gocql.Session, error) {

	cluster := gocql.NewCluster(config.AllConfigs.ScyllaDB.ConnectionIp...)
	cluster.Consistency = gocql.Quorum
	cluster.Keyspace = config.AllConfigs.ScyllaDB.KeySpace
	cluster.ProtoVersion = 4
	cluster.SocketKeepalive = 10 * time.Second

	client, err := cluster.CreateSession()
	if err != nil {
		return nil, errors.ScyllaDBConnectError.DevMessage(err.Error())
	}

	return client, nil
}
