package main

import (
	"flag"
	"fmt"
	"gitlab.com/golang-demos/blog-api/src/blog"
	"gitlab.com/golang-demos/blog-api/src/config"
	"gitlab.com/golang-demos/blog-api/src/repository/elastic"
	"gitlab.com/golang-demos/blog-api/src/repository/scylladb"
	"net/http"
	"os"
	"os/signal"
	"syscall"
)

// Init vars for services
var (
	blogService blog.Service
)

// Main blog-api function
func main() {

	// Set http port
	httpAddr := flag.String("http.addr", ":8080", "HTTP listen address only port :8080")
	flag.Parse()

	// Get configs
	err := config.GetConfigs()
	if err != nil {
		panic(fmt.Errorf("Fatal error config file: %s \n", err))
	}

	// Init connections
	scyllaDBConnection, err := scylladb.ConnectionStart()
	if err != nil {
		panic(fmt.Errorf("ScyllaDB connection error: %s \n", err))
	}
	defer scyllaDBConnection.Close()

	elasticConnection, err := elastic.ConnectionStart()
	if err != nil {
		panic(fmt.Errorf("Elastic connection error: %s \n", err))
	}

	blogCommandRepo := scylladb.NewPostCommandRepo(scyllaDBConnection)
	blogQueryRepo := elastic.NewPostQueryRepo(elasticConnection)

	// Init blog service
	blogService = blog.NewService(
		blogCommandRepo,
		blogQueryRepo,
	)

	mux := http.NewServeMux()
	mux.Handle("/blog-api/", blog.MakeHandler(blogService))

	http.Handle("/blog-api/", accessControl(mux))

	errs := make(chan error, 2)

	// Init http serve
	go func() {
		errs <- http.ListenAndServe(*httpAddr, nil)
	}()

	// Listen errors chan
	go func() {
		c := make(chan os.Signal)
		signal.Notify(c, syscall.SIGINT)
		errs <- fmt.Errorf("%s", <-c)
	}()

	fmt.Println("terminated", <-errs)
}

// Additional structures & functions
func accessControl(h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

		w.Header().Set("Access-Control-Allow-Origin", "*")
		w.Header().Set("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS")
		w.Header().Set("Access-Control-Allow-Headers", "Origin, Content-Type, Authorization,X-Owner,darvis-dialog-id")

		if r.Method == "OPTIONS" {
			return
		}

		h.ServeHTTP(w, r)
	})
}
