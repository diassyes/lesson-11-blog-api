module gitlab.com/golang-demos/blog-api

go 1.13

require (
	github.com/go-kit/kit v0.10.0
	github.com/gocql/gocql v0.0.0-20200228163523-cd4b606dd2fb
	github.com/golang/protobuf v1.3.4 // indirect
	github.com/golang/snappy v0.0.1 // indirect
	github.com/google/uuid v1.1.1
	github.com/gorilla/mux v1.7.4
	github.com/konsorten/go-windows-terminal-sequences v1.0.2 // indirect
	github.com/olivere/elastic/v7 v7.0.12
	github.com/prometheus/client_golang v1.5.0
	github.com/prometheus/procfs v0.0.10 // indirect
	github.com/sirupsen/logrus v1.4.2
	golang.org/x/sys v0.0.0-20200302150141-5c8b2ff67527 // indirect
)
